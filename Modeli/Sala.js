const Sequelize = require("sequelize");
module.exports = function(sequelize,DataTypes){
    const Sala = sequelize.define("sala", {
        id: {
            type: Sequelize.INTEGER,
            allowNull: false,
            unique: true,
            primaryKey: true,
            autoIncrement: true
        },
        naziv: Sequelize.STRING,
        zaduzenaOsoba: {
            type: Sequelize.INTEGER,
            references: {
                model: 'Osoblje',
                key: 'id'
            }
        }

    }, { freezeTableName: true, timestamps: false }) //Sprijecava promjenu imena tabele u mnozinu, kao i upis suvisnih kolona 
    return Sala;
};
