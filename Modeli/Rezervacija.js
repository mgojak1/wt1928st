const Sequelize = require("sequelize");
module.exports = function(sequelize,DataTypes){
    const Rezervacija = sequelize.define("rezervacija", {
        id: {
            type: Sequelize.INTEGER,
            allowNull: false,
            unique: true,
            primaryKey: true,
            autoIncrement: true
        },
        termin: {
            type: Sequelize.INTEGER,
            references:{ 
                model: 'Termin'
            }
        },
        sala: {
            type: Sequelize.INTEGER,
            references: {
                model: 'Sala',
                key: 'id'
            }
        },
        osoba: {
            type: Sequelize.INTEGER,
            references: {
                model: 'Osoblje',
                key: 'id'
            }
        }
    }, { freezeTableName: true, timestamps: false }) //Sprijecava promjenu imena tabele u mnozinu, kao i upis suvisnih kolona
    return Rezervacija;
};
