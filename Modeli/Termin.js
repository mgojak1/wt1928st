const Sequelize = require("sequelize");
module.exports = function(sequelize,DataTypes){
    const Termin = sequelize.define("termin", {
        id: {
            type: Sequelize.INTEGER,
            allowNull: false,
            unique: true,
            primaryKey: true,
            autoIncrement: true
        },
        redovni: Sequelize.BOOLEAN,
        dan: Sequelize.INTEGER,
        datum: Sequelize.STRING,
        semestar: Sequelize.STRING,
        pocetak: Sequelize.TIME,
        kraj: Sequelize.TIME
    }, { freezeTableName: true, timestamps: false }) //Sprijecava promjenu imena tabele u mnozinu, kao i upis suvisnih kolona
    return Termin;
};
