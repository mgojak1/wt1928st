const Sequelize = require("sequelize");
module.exports = function(sequelize,DataTypes){
    const Osoblje = sequelize.define("osoblje", {
        id: {
            type: Sequelize.INTEGER,
            allowNull: false,
            unique: true,
            primaryKey: true,
            autoIncrement: true
        },
        ime: Sequelize.STRING,
        prezime: Sequelize.STRING,
        uloga: Sequelize.STRING
    }, { freezeTableName: true, timestamps: false }) //Sprijecava promjenu imena tabele u mnozinu, kao i upis suvisnih kolona
    return Osoblje;
};
