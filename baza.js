const Sequelize = require("sequelize");
const sequelize = new Sequelize("DBWT19","root","root",{ host:'127.0.0.1', dialect:"mysql", logging: false });
//Za rad na lokalnoj bazi
//const sequelize = new Sequelize("DBWT19","root","root",{ host:'baza.db', dialect:"sqlite", logging: false });

const db = {};
db.Sequelize = Sequelize;  
db.sequelize = sequelize;

//Import modela
db.osoblje = sequelize.import(__dirname + '/Modeli/Osoblje.js');
db.sala = sequelize.import(__dirname + '/Modeli/Sala.js');
db.termin = sequelize.import(__dirname + '/Modeli/Termin.js');
db.rezervacija = sequelize.import(__dirname + '/Modeli/Rezervacija.js');

//Asocijacija relacija izmedju modela
//Radi se u parovima prema Sequelize dokumentaciji

//Sala - osoblje
db.sala.belongsTo(db.osoblje, { as: 'osoba', foreignKey: 'zaduzenaOsoba' });
db.osoblje.hasOne(db.sala, { foreignKey: 'zaduzenaOsoba' });
//Rezervacija - termin
db.rezervacija.belongsTo(db.termin, { as: 'vrijeme', foreignKey:'termin' });
db.termin.hasOne(db.rezervacija, { foreignKey: 'termin' });
//Rezervacija - sala
db.sala.hasMany(db.rezervacija, { foreignKey: 'sala' });
db.rezervacija.belongsTo(db.sala, { as: 'gdje', foreignKey: 'sala' });
//Osoblje - rezervacija
db.osoblje.hasMany(db.rezervacija, { foreignKey: 'osoba' });
db.rezervacija.belongsTo(db.osoblje, { as: 'ko',foreignKey: 'osoba'});

module.exports = db;