const express = require("express");
const app = express();
const init = require('./inicijalizacija.js');
const db = require('./baza.js');

//Inicijalizira bazu na pocetno stanje sa svakim pokretanjem
//Nije potrebno rucno pokretati baza.js i inicijalizacija.js
init.initialize();

//Omogucava upotrebu potrebnih fajlova
app.use(express.static(__dirname + '/public'));
app.use(express.static(__dirname + '/test'));

//GET za pocetnu
app.get("/", function (req, res) {
    res.sendFile(__dirname + "/public/pocetna.html");
});

//GET ruta za osoblje - Zadatak 1
app.get('/osoblje', function (req, res) {
    db.osoblje.findAll({ raw: true }).then(function (osoblje) {
        res.send(osoblje);
        res.end();
    });
})

//GET ruta koja vraca objekat sa imenom i lokacijom osobe za trenutno vrijeme
app.get('/osobljeLokacija', function (req, res) {
    db.osoblje.findAll({
        raw: true, include: [{
            model: db.rezervacija,
            required: false,
            include: [{
                model: db.termin, required: false, as: 'vrijeme'
            }, {
                model: db.sala, required: false, as: 'gdje'
            }]
        }]
    }).then(function (osobljeLokacija) {
        let podaci = [];
        osobljeLokacija.forEach(element => {
            let tmp = {
                ime: element['ime'] + ' ' + element['prezime'],
                uloga: element['uloga'],
                sala: 'Kancelarija'
            };
            //Serverska provjera postojanja zauzeca
            let trenutnoVrijeme = new Date(Date.now());
            trenutnoVrijeme.setFullYear(2019);
            //Redovne rezervacije
            if (element['rezervacijas.vrijeme.redovni'] == 0) {
                let vrijeme = element['rezervacijas.vrijeme.datum'];
                let elementDatum = new Date(parseInt(vrijeme.slice(6)),
                    parseInt(vrijeme.slice(3, 5)) - 1, parseInt(vrijeme.slice(0, 3)));
                //Datum i vrijeme odgovaraju
                if (elementDatum.getFullYear() == trenutnoVrijeme.getFullYear() &&
                    elementDatum.getMonth() == trenutnoVrijeme.getMonth() &&
                    elementDatum.getDate() == trenutnoVrijeme.getDate()) {
                    if (parseInt(element['rezervacijas.vrijeme.pocetak']) <= trenutnoVrijeme.getHours() &&
                        parseInt(element['rezervacijas.vrijeme.kraj']) >= trenutnoVrijeme.getHours()) {
                        tmp['sala'] = element['rezervacijas.gdje.naziv'];
                    }
                }//Vanredne rezervacije
            } else if (element['rezervacijas.vrijeme.redovni'] == 1) {
                //Dan u sedmici i vrijeme odgovaraju
                //Date objekat racuna nedelju kao prvi dan u sedmici 
                if (trenutnoVrijeme.getDay() == (element['rezervacijas.vrijeme.dan'] + 1) % 7) {
                    if (parseInt(element['rezervacijas.vrijeme.pocetak']) <= trenutnoVrijeme.getHours() &&
                        parseInt(element['rezervacijas.vrijeme.kraj']) >= trenutnoVrijeme.getHours()) {
                        tmp['sala'] = element['rezervacijas.gdje.naziv'];
                    }
                }
            }
            //Provjera da li je ista osoba vec dodana sa 'Kancelarija'
            let dodaj = true;
            podaci.forEach(podatak => {
                if (podatak.ime == tmp.ime) {
                    if (podatak.sala == 'Kancelarija' || tmp.sala != 'Kancelarija') {
                        podatak.sala = tmp.sala
                        dodaj = false;
                    }
                }
            });
            if (dodaj) podaci.push(tmp);
        });
        res.send(podaci);
        res.end();
    });
})

//GET ruta za sale
app.get('/sale', function (req, res) {
    db.sala.findAll({ raw: true }).then(function (sale) {
        res.send(sale);
        res.end();
    });
})

//GET ruta za zauzeca
app.get("/zauzeca", function (req, res) {
    db.rezervacija.findAll({
        raw: true, include: [{
            model: db.termin,
            required: true,
            as: 'vrijeme'
        }, {
            model: db.sala,
            required: true,
            as: 'gdje'

        }, {
            model: db.osoblje,
            required: true,
            as: 'ko'

        }]
    }).then(function (rezervacije) {
        let podaci = { periodicna: [], vanredna: [] };
        rezervacije.forEach(rezervacija => {
            let tmp = {
                naziv: rezervacija['gdje.naziv'],
                pocetak: rezervacija['vrijeme.pocetak'],
                kraj: rezervacija['vrijeme.kraj'],
                predavac: rezervacija['ko.ime'] + ' ' + rezervacija['ko.prezime']

            };
            if (rezervacija['vrijeme.redovni'] == 0) {
                tmp['datum'] = rezervacija['vrijeme.datum'];
                podaci.vanredna.push(tmp)
            } else {
                tmp['dan'] = rezervacija['vrijeme.dan'];
                tmp['semestar'] = rezervacija['vrijeme.semestar'];
                podaci.periodicna.push(tmp);
            }
        });
        res.send(podaci);
        res.end();
    })
});

//GET za rezervacije
app.get("/rezervacije", function (req, res) {
    res.sendFile(__dirname + "/public/rezervacije.html");
});

//Obrada POST zahtjeva za upis u zauzeca - Spirala 4
app.post("/rezervacije.html", function (req, res) {
    let podaci = '';
    req.on('data', function (data) {
        podaci += data;
    })
    req.on('end', function () {
        let newObj = JSON.parse(podaci);
        let periodicnost = newObj["periodicnost"];
        delete newObj["periodicnost"];
        db.rezervacija.findAndCountAll({
            raw: true,
            include: [{
                model: db.termin,
                required: true,
                as: 'vrijeme',
            }, {
                model: db.sala,
                required: true,
                as: 'gdje',
                where: {
                    naziv: newObj['naziv']
                }
            }, {
                model: db.osoblje,
                required: true,
                as: 'ko'

            }]
        }).then(function (rezervacije) {
            //Serverska provjera postojanja zauzeca
            let slobodna = true;
            if (rezervacije.count != 0) {
                rezervacije.rows.forEach(rezervacija => {
                    //Redovni termini
                    if (rezervacija['vrijeme.redovni'] == 0) {
                        if (rezervacija['vrijeme.datum'] == newObj['datum']) {
                            if (!((parseInt(rezervacija["vrijeme.pocetak"]) <= parseInt(newObj["pocetak"]) &&
                                parseInt(rezervacija["vrijeme.kraj"]) <= parseInt(newObj["pocetak"])) ||
                                (parseInt(rezervacija["vrijeme.pocetak"]) >= parseInt(newObj["kraj"]) &&
                                    parseInt(rezervacija["vrijeme.kraj"]) >= parseInt(newObj["kraj"])))) {
                                res.status(500).send({ osoba: rezervacija['ko.ime'] + ' ' + rezervacija['ko.prezime'] }).end();
                                slobodna = false;
                            }
                        }
                    } else { //Vanredni termini
                        let datum = new Date(parseInt(newObj["datum"].slice(6)),
                            parseInt(newObj["datum"].slice(3, 5)) - 1, parseInt(newObj["datum"].slice(0, 3)));
                        if (parseInt(rezervacija["vrijeme.dan"]) + 1 == parseInt(datum.getDay())) {
                            if ((rezervacija["vrijeme.semestar"] == "zimski" && (parseInt(newObj["datum"].slice(3, 5)) - 1 == 0 ||
                                parseInt(newObj["datum"].slice(3, 5)) - 1 > 8)) ||
                                (rezervacija["vrijeme.semestar"] == "ljetni" && (parseInt(newObj["datum"].slice(3, 5)) - 1 >= 1 &&
                                    parseInt(newObj["datum"].slice(3, 5)) - 1 <= 5))) {
                                if (!((parseInt(rezervacija["vrijeme.pocetak"]) <= parseInt(newObj["pocetak"]) &&
                                    parseInt(rezervacija["vrijeme.kraj"]) <= parseInt(newObj["pocetak"])) ||
                                    (parseInt(rezervacija["vrijeme.pocetak"]) >= parseInt(newObj["kraj"]) &&
                                        parseInt(rezervacija["vrijeme.kraj"]) >= parseInt(newObj["kraj"])))) {
                                    res.status(500).send({ osoba: rezervacija['ko.ime'] + ' ' + rezervacija['ko.prezime'] }).end();
                                    slobodna = false;
                                }
                            }
                        }
                    }
                });
            }
            //Dodavanje u bazu
            if (slobodna) {
                //Ako nije ni zmiski ni ljetni semestar zauzece mora biti vanredno - dodatni komentar asistenta
                let datum = new Date(parseInt(newObj["datum"].slice(6)),
                    parseInt(newObj["datum"].slice(3, 5)) - 1, parseInt(newObj["datum"].slice(0, 3)));
                /*Vanredna zauzeca ( Ako je datum u vremenu ljetnog raspusta 
                zauzece mora biti vanredno - komentar asistenta na Spirali 3)*/
                if (!periodicnost || (datum.getMonth() >= 5 && datum.getMonth() <= 8)) {
                    //Ubacivanje termina
                    db.termin.create({
                        redovni: false,
                        datum: newObj['datum'],
                        pocetak: newObj['pocetak'],
                        kraj: newObj['kraj']
                    }).then(function (termin) {
                        //Povezivanje sale i osobe
                        db.sala.findOne({ where: { naziv: newObj['naziv'] } }).then(function (sala) {
                            let imePrezime = newObj['predavac'].split(' ');
                            db.osoblje.findOne({ where: { ime: imePrezime[0], prezime: imePrezime[1] } }).then(function (osoba) {
                                //Ubacivanje rezervacije
                                db.rezervacija.create({}).then(function (rezervacija) {
                                    rezervacija.setVrijeme(termin);
                                    rezervacija.setGdje(sala);
                                    rezervacija.setKo(osoba);
                                    return new Promise(function (resolve, reject) { resolve(rezervacija); res.status(200).end(); });
                                });
                            });
                        });
                    });
                } else {
                    //Redovna zauzeca
                    let semestar = "zimski";
                    if (datum.getMonth() >= 1 && datum.getMonth() <= 5) semestar = "ljetni";
                    //Ubacivanje termina
                    db.termin.create({
                        redovni: true,
                        dan: (datum.getDay() - 1) % 7,
                        semestar: semestar,
                        pocetak: newObj['pocetak'],
                        kraj: newObj['kraj']
                    }).then(function (termin) {
                        //Povezivanje sale i osobe
                        db.sala.findOne({ where: { naziv: newObj['naziv'] } }).then(function (sala) {
                            let imePrezime = newObj['predavac'].split(' ');
                            db.osoblje.findOne({ where: { ime: imePrezime[0], prezime: imePrezime[1] } }).then(function (osoba) {
                                //Ubacivanje rezervacije
                                db.rezervacija.create({}).then(function (rezervacija) {
                                    rezervacija.setVrijeme(termin);
                                    rezervacija.setGdje(sala);
                                    rezervacija.setKo(osoba);
                                    return new Promise(function (resolve, reject) { resolve(rezervacija); res.status(200).end(); });
                                });
                            });
                        });
                    });
                }
            } else {
                //Greska
                res.status(500).end();
            }
        });
    })
});

//Prikaz greske za nepostojece putanje
app.get(/[a-z]*/, function (req, res) {
    res.send("Error 404 - page not found");
});

app.listen(8080);
console.log("Server port: 8080");