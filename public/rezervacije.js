//Pocetno asinhrono ucitavanje zauzeca i osoba
Pozivi.ucitajSale();
Pozivi.ucitajOsobe();
Pozivi.ucitajZauzeca();

//Omogucava odabir celije kalendara
$("td.datum").click(function () {
    if ($(this).text() != "") {
        let mjesec = trenutni_mjesec + 1;
        if (confirm("Da li zelite rezervisati salu "
            + sale.options[sale.selectedIndex].value + " za " + $(this).text() + "/" + mjesec + "/2019" +
            " u terminu " + input_pocetak.value + "-" + input_kraj.value + " ?")) {
            let dan = $(this).text();
            if (dan.length == 1) dan = "0" + dan; //Format datuma mora biti dd.mm.yyyy
            if (mjesec < 10) mjesec = "0" + mjesec;
            Pozivi.upisiZauzece(sale.options[sale.selectedIndex].value, dan + "." + mjesec + ".2019",
                input_pocetak.value, input_kraj.value, osobe.options[osobe.selectedIndex].value,periodicnost.checked);
        }
    }
});

