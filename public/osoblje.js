$(document).ready(
    ucitajOsoblje()
);
//Periodicno pozivanje svakih 30s
function ucitajOsoblje() {
    $.ajax({
        url: "/osobljeLokacija", success: function (data) {
            let podaci = data;
            $('#tabelaOsoba > tbody > tr:not(".headRow")').remove();
            $.each(podaci, function (i, osoba) {
                $('#tabelaOsoba > tbody:last-child').append('<tr>'
                   + '<td>' + osoba.ime + '</td>'
                   + '<td>' + osoba.uloga + '</td>'
                   + '<td>' + osoba.sala + '</td>'
                   + '</tr>');
            });
        }, complete : function () {
            setTimeout(ucitajOsoblje, 30000);
        }
    })
}