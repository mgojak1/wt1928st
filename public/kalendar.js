//Globalne varijable
var trenutni_mjesec = 0;
//if(typeof trenutni_mjesec !== undefined) trenutni_mjesec = 10;
var mjeseci = ["Januar", "Februar", "Mart", "April", "Maj", "Juni", "Juli", 
"Avgust", "Septembar", "Oktobar", "Novembar", "Decembar"];
var podaci = { periodicni: [], vanredni: [] };

//Header kalendara
var naziv_mjeseca = document.getElementById("naziv_mjeseca");
naziv_mjeseca.innerHTML = mjeseci[trenutni_mjesec];

//Dropdown lista za sale
var sale = document.getElementById("lista_sala");

//Dropdown lista za osobe
var osobe = document.getElementById("lista_osoba");

//Checkbox za periodicnos
var periodicnost = document.getElementById("periodicno");

//Input polja za vrijeme
var input_pocetak = document.getElementById("pocetak");
var input_kraj = document.getElementById("kraj");
input_pocetak.value = "11:00";
input_kraj.value = "14:00";

//Modul Kalendar
let Kalendar = (function () {

    function ucitajPodatkeImpl(periodicna, vanredna) {
        podaci = { periodicni: [], vanredni: [] };
        periodicna.forEach(element => {
            podaci.periodicni.push(element);
        });
        vanredna.forEach(element => {
            podaci.vanredni.push(element);
        });
        return podaci;
    }
    //Pomocne funkcije
    function obojiPeriodicne(mjesec, sala, predavac) {
        podaci.periodicni.forEach(element => {
            if (element.naziv == sala && element.predavac == predavac) {
                if ((element.semestar == "zimski" && (mjesec == 0 || mjesec > 8)) ||
                    (element.semestar == "ljetni" && (mjesec >= 1 && mjesec <= 5))) {
                    var dani; // Kolone kalendara po danima
                    if (element.dan == 0) dani = document.getElementsByClassName("pon");
                    else if (element.dan == 1) dani = document.getElementsByClassName("uto");
                    else if (element.dan == 2) dani = document.getElementsByClassName("sri");
                    else if (element.dan == 3) dani = document.getElementsByClassName("cet");
                    else if (element.dan == 4) dani = document.getElementsByClassName("pet");
                    else if (element.dan == 5) dani = document.getElementsByClassName("sub");
                    else dani = document.getElementsByClassName("ned");
                    if (!((parseInt(input_pocetak.value) <= parseInt(element.pocetak) &&
                        parseInt(input_kraj.value) <= parseInt(element.pocetak)) ||
                        (parseInt(input_pocetak.value) >= parseInt(element.kraj) &&
                            parseInt(input_kraj.value) >= parseInt(element.kraj)))) {
                        for (let i = 0; i < dani.length; i++) {
                            if (dani[i].innerHTML != "") dani[i].classList.add("red");
                        }
                    }
                }
            }
        });
    }

    function obojiVanredne(mjesec, sala, predavac) {
        podaci.vanredni.forEach(element => {
            if (element.naziv == sala && element.predavac == predavac) {
                var datum = new Date(parseInt(element.datum.slice(6)),
                    parseInt(element.datum.slice(3, 5)) - 1,
                    parseInt(element.datum.slice(0, 3)));
                if (mjesec == datum.getMonth()) {
                    var dani = document.getElementsByClassName("datum"); //Celije kalendara
                    if (!((parseInt(input_pocetak.value) <= parseInt(element.pocetak) &&
                        parseInt(input_kraj.value) <= parseInt(element.pocetak)) ||
                        (parseInt(input_pocetak.value) >= parseInt(element.kraj) &&
                            parseInt(input_kraj.value) >= parseInt(element.kraj)))) {
                        for (let i = 0; i < dani.length; i++) {
                            if (parseInt(dani[i].innerHTML) == datum.getDate()) {
                                dani[i].classList.add("red");
                            }
                        }
                    }
                }
            }
        });
    }

    //Funkcija boji sve zauzete sate a ne samo primljene
    //zato se salje null
    //Ne vidim potrebu za referencom na kalendar pa nije ni koristena
    function obojiZauzecaImpl(kalendarRef, mjesec, sala, pocetak, kraj, predavac) {
        var tmp = document.getElementsByClassName("datum"); //Celije kalendara
        //Resetuje stilove na default
        for (let i = 0; i < tmp.length; i++) {
            tmp[i].classList.add("green");
            tmp[i].classList.remove("red");
            if (tmp[i].innerHTML == "") tmp[i].classList.remove("green");
            //if(mjesec >= 6 && mjesec <= 8) tmp[i].classList.remove("green");              
        }
        if (periodicnost.checked == true) obojiPeriodicne(mjesec, sala, predavac);
        obojiVanredne(mjesec, sala, predavac);
    }

    function sljedeciMjesecImpl() {
        if (trenutni_mjesec < 11) trenutni_mjesec++;
        naziv_mjeseca.innerHTML = mjeseci[trenutni_mjesec];
        iscrtajKalendarImpl(null, trenutni_mjesec);
        obojiZauzecaImpl(null, trenutni_mjesec, sale.options[sale.selectedIndex].text,
            null, null, osobe.options[osobe.selectedIndex].text);
        return false;
    }

    function prethodniMjesecImpl() {
        if (trenutni_mjesec > 0) trenutni_mjesec--;
        naziv_mjeseca.innerHTML = mjeseci[trenutni_mjesec];
        iscrtajKalendarImpl(null, trenutni_mjesec);
        obojiZauzecaImpl(null, trenutni_mjesec, sale.options[sale.selectedIndex].text,
            null, null, osobe.options[osobe.selectedIndex].text);
        return false;
    }

    function iscrtajKalendarImpl(kalendarRef, mjesec) {
        var dan = document.getElementsByClassName("datum");
        var date = new Date(2019, mjesec, 0);
        var pocetak = date.getDay();
        date.setDate(date.getDate() + 1);
        for (let index = 0; index < dan.length; index++) {
            dan[index].innerHTML = "";
        }
        var tmp = date.getMonth();
        for (let index = 0; index < dan.length - pocetak; index++) {
            if (date.getMonth() != tmp) {
                date.setDate(date.getDate() - 1);
                break;
            }
            dan[index + pocetak].innerHTML = date.getDate();
            date.setDate(date.getDate() + 1);
        }
    }

    return {
        obojiZauzeca: obojiZauzecaImpl,
        sljedeciMjesec: sljedeciMjesecImpl,
        prethodniMjesec: prethodniMjesecImpl,
        iscrtajKalendar: iscrtajKalendarImpl,
        ucitajPodatke: ucitajPodatkeImpl
    }
}());

//Metode za dugmice
var sljedeci = Kalendar.sljedeciMjesec;
var prethodni = Kalendar.prethodniMjesec;

//Listeneri za input polja
sale.addEventListener("change", function () {
    Kalendar.obojiZauzeca(null, trenutni_mjesec, sale.options[sale.selectedIndex].text,
        null, null, osobe.options[osobe.selectedIndex].text);
});

periodicnost.addEventListener("change", function () {
    Kalendar.obojiZauzeca(null, trenutni_mjesec, sale.options[sale.selectedIndex].text,
        null, null, osobe.options[osobe.selectedIndex].text);
});

input_pocetak.addEventListener("change", function () {
    Kalendar.obojiZauzeca(null, trenutni_mjesec, sale.options[sale.selectedIndex].text,
        null, null, osobe.options[osobe.selectedIndex].text);
});

input_kraj.addEventListener("change", function () {
    Kalendar.obojiZauzeca(null, trenutni_mjesec, sale.options[sale.selectedIndex].text,
        null, null, osobe.options[osobe.selectedIndex].text);
});

osobe.addEventListener("change", function () {
    Kalendar.obojiZauzeca(null, trenutni_mjesec, sale.options[sale.selectedIndex].text,
        null, null, osobe.options[osobe.selectedIndex].text);
});

//Inicijalno bojenje
Kalendar.iscrtajKalendar(null, trenutni_mjesec);