//URL slika za ucitavanje
let url1 = '/slike/bobber.jpeg';
let url2 = '/slike/bolt.jpg';
let url3 = '/slike/bonevile.jpeg';
let url4 = '/slike/goldwing.jpg';
let url5 = '/slike/gs.jpeg';
let url6 = '/slike/h2r.jpg';
let url7 = '/slike/iron.jpeg';
let url8 = '/slike/rebel.jpg';
let url9 = '/slike/scout.jpeg';
let url10 = '/slike/ural.jpg';
let broj = 0;

//Ucitavanje prve tri slike pomocu ajax poziva
$(document).ready(function () {
    Pozivi.ucitajSlike(url1, url2, url3);
    broj++;
});

//Ucitavanje sljedecih pomocu ajax poziva
//Ako se klikne na prethodni pa sljedeci slike se opet ucitavaju pomoc ajaxa jer postavka zadatka to trazi
function sljedece_slike() {
    if(broj < 4) broj++;
    else return false;
    if(broj == 2) Pozivi.ucitajSlike(url4, url5, url6);
    if(broj == 3) Pozivi.ucitajSlike(url7, url8, url9);
    $("#prva").show().fadeOut();
    $("#druga").show().fadeOut();
    $("#treca").show().fadeOut();
    if(broj == 4) {
        Pozivi.ucitajSlike(url10, '', '');
        $("#druga").show().fadeOut();
        $("#treca").show().fadeOut();
    }
    return false;
}

//Ucitavanje prethodnih slika bez ajax poziva
function prethodne_slike() {
    if(broj > 1) broj--;
    else return false;
    if (broj == 1) {
        $("#prva").attr("src", url1).hide().fadeIn();
        $("#druga").attr("src", url2).hide().fadeIn();
        $("#treca").attr("src", url3).hide().fadeIn();
    }

    if (broj == 2) {
        $("#prva").attr("src", url4).hide().fadeIn();
        $("#druga").attr("src", url5).hide().fadeIn();
        $("#treca").attr("src", url6).hide().fadeIn();
    }
    if (broj == 3) {
        $("#prva").attr("src", url7).hide().fadeIn();
        $("#druga").attr("src", url8).hide().fadeIn();
        $("#treca").attr("src", url9).hide().fadeIn();
    }
    return false;
}
