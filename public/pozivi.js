let Pozivi = function () {

    //Asinhrona metoda za ucitavanje zauzeca preko jQuery-a
    //Server salje zauzeca.json preko URL-a /zauzeca - dio prvog zadatka
    function ucitajZauzeca() {
        $.ajax({
            url: "/zauzeca", dataType: "json", success: function (data) {
                let podaci = data;
                Kalendar.ucitajPodatke(podaci["periodicna"], podaci["vanredna"]);
                Kalendar.obojiZauzeca(null, trenutni_mjesec, sale.options[sale.selectedIndex].text,
                    null, null, osobe.options[osobe.selectedIndex].text)
            }
        });
    };

    function ucitajOsobe() {
        $.ajax({
            url: "/osoblje", success: function (data) {
                let podaci = data;
                $.each(podaci, function (i, osoba) {
                    $('#lista_osoba').append($('<option>', {
                        value: osoba.ime + ' ' + osoba.prezime,
                        text: osoba.ime + ' ' + osoba.prezime
                    }));
                });
            }
        });
    };

    function ucitajSale() {
        $.ajax({
            url: "/sale", success: function (data) {
                let podaci = data;
                $('#lista_sala option').remove();
                $.each(podaci, function (i, sala) {
                    $('#lista_sala').append($('<option>', {
                        value: sala.naziv,
                        text: sala.naziv
                    }));
                });
            }
        });
    };

    //Salje zahtijev serveru za rezervaciju sale
    //Na uspjesan upis prikazuje i boji zauzeca
    //U slucaju greske prikazuje odgovarajucu poruku
    function upisiZauzece(sala, datum, pocetak, kraj, predavac, periodicnost) {
        let podaci = { datum: datum, pocetak: pocetak, kraj: kraj, naziv: sala, predavac: predavac, periodicnost: periodicnost }
        podaci = JSON.stringify(podaci);
        $.ajax({
            type: 'POST', url: "/rezervacije.html", data: podaci, success: function () {
                ucitajZauzeca();
            }, error: function (request, status, error) {
                let osoba = request.responseJSON;
                alert("Salu je zauzeo " + osoba.osoba + " u tom terminu!");
            }
        });
    }

    //Ajax poziv za ucitavanje tri slike u odgovarajuce elemente
    //Ako je url2 i url3 prazan nema vise slika za prikazati pa se elementi nece ucitavati
    function ucitajSlike(url1, url2, url3) {
        $.ajax({
            url: url1,
            cache: true,
            processData: false,
        }).always(function () {
            $("#prva").attr("src", url1).fadeIn();
        });
        if (url2 != '' && url3 != '') {
            $.ajax({
                url: url2,
                cache: true,
                processData: false,
            }).always(function () {
                $("#druga").attr("src", url2).fadeIn();
            });
            $.ajax({
                url: url3,
                cache: true,
                processData: false,
            }).always(function () {
                $("#treca").attr("src", url3).fadeIn();
            });
        }
    }
    return {
        ucitajZauzeca: ucitajZauzeca,
        upisiZauzece: upisiZauzece,
        ucitajSlike: ucitajSlike,
        ucitajOsobe: ucitajOsobe,
        ucitajSale: ucitajSale
    }
}();
