const chai = require('chai');
const expect = require('chai').expect;
const assert = require('chai').assert;
const request = require('request');

//Testovi podrazumijevaju da je baza u pocetnom stanju
//Za pokretanje testova pokrenite 'node index.js', a zatim 'npm test'!
describe('Testovi serverske funkcionalnosti', function () {
    describe('GET /osoblje', function () {
        it('Test 1: Broj vracenih osoba: ', function (done) {
            try {
                request('http://localhost:8080/osoblje', function (error, response, body) {
                    let osoblje = JSON.parse(body);
                    expect(osoblje).to.have.lengthOf(3);
                    done();
                });
            }
            catch (error) {
                done(error);
            }
        });
        it('Test 2: Imena vracenih osoba: ', function (done) {
            try {
                request('http://localhost:8080/osoblje', function (error, response, body) {
                    let osoblje = JSON.parse(body);
                    for (let i = 0; i < 3; i++) {
                        if (i == 0) assert.equal(osoblje[i].ime, 'Neko', 'Prva osoba je "Neko Nekic"');
                        if (i == 1) assert.equal(osoblje[i].ime, 'Drugi', 'Druga osoba je "Drugi Neko"');
                        if (i == 2) assert.equal(osoblje[i].ime, 'Test', 'Treca osoba je "Test Test"');
                    }
                    expect(osoblje).to.have.lengthOf(3);
                    done();
                });
            } catch (error) {
                done(error);
            }
        });
    });
    describe('Dohvacanje svih sala', function () {
        it('Test 3: Broj vracenih sala: ', function (done) {
            try {
                request('http://localhost:8080/sale', function (error, response, body) {
                    let sale = JSON.parse(body);
                    expect(sale).to.have.lengthOf(2);
                    done();
                });
            } catch (error) {
                done(error);
            }
        });
        it('Test 4: Nazivi vracenih sala: ', function (done) {
            try {
                request('http://localhost:8080/sale', function (error, response, body) {
                    let sale = JSON.parse(body);
                    for (let i = 0; i < 2; i++) {
                        if (i == 0) assert.equal(sale[i].naziv, '1-11', 'Prva sala je "1-11"');
                        if (i == 1) assert.equal(sale[i].naziv, '1-15', 'Druga sala je "1-15"');
                    }
                    expect(sale).to.have.lengthOf(2);
                    done();
                });
            } catch (error) {
                done(error);
            }
        });
    });
    describe('Dohvacanje zauzeca', function () {
        it('Test 5: Broj vracenih zauzeca: ', function (done) {
            try {
                request('http://localhost:8080/zauzeca', function (error, response, body) {
                    let zauzeca = JSON.parse(body);
                    expect(zauzeca.periodicna).to.have.lengthOf(1);
                    expect(zauzeca.vanredna).to.have.lengthOf(1);
                    done();
                });
            } catch (error) {
                done(error);
            }
        });
        it('Test 6: Broj vracenih zauzeca nakon dodavanja novog zauzeca: ', function (done) {
            let zauzece = {
                datum: '11.12.2019',
                pocetak: '12:00',
                kraj: '13:00',
                naziv: '1-15',
                predavac: 'Test Test',
                periodicnost: false
            }
            try {
                request({
                    url: 'http://localhost:8080/rezervacije.html',
                    method: "POST",
                    headers: {
                        "content-type": "application/json",
                    },
                    body: zauzece,
                    json: true
                }, function (error, response, body) {
                    request('http://localhost:8080/zauzeca', function (error, response, body) {
                        let zauzeca = JSON.parse(body);
                        expect(zauzeca.periodicna).to.have.lengthOf(1);
                        expect(zauzeca.vanredna).to.have.lengthOf(2);
                        done();
                    })
                })
            } catch (error) {
                done(error);
            }
        });
    });
    describe('Rezervacija novih termina', function () {
        it('Test 7: Ubacivanje vanrednog zauzeca: ', function (done) {
            let zauzece = {
                datum: '12.12.2019',
                pocetak: '17:00',
                kraj: '19:00',
                naziv: '1-15',
                predavac: 'Test Test',
                periodicnost: false
            }
            try {
                request({
                    url: 'http://localhost:8080/rezervacije.html',
                    method: "POST",
                    headers: {
                        "content-type": "application/json",
                    },
                    body: zauzece,
                    json: true
                }, function (error, response, body) {
                    request('http://localhost:8080/zauzeca', function (error, response, body) {
                        let zauzeca = JSON.parse(body);
                        delete zauzece['periodicnost'];
                        expect(zauzeca.vanredna[zauzeca.vanredna.length-1]).to.eql(zauzece);
                        done();
                    })
                })
            } catch (error) {
                done(error);
            }
        });
        it('Test 8: Ubacivanje redovnog zauzeca: ', function (done) {
            let zauzece = {
                datum: '05.03.2019',
                pocetak: '09:00',
                kraj: '10:00',
                naziv: '1-11',
                predavac: 'Drugi Neko',
                periodicnost: true
            }
            try {
                request({
                    url: 'http://localhost:8080/rezervacije.html',
                    method: "POST",
                    headers: {
                        "content-type": "application/json",
                    },
                    body: zauzece,
                    json: true
                }, function (error, response, body) {
                    request('http://localhost:8080/zauzeca', function (error, response, body) {
                        let zauzeca = JSON.parse(body);
                        expect(zauzeca.periodicna).to.have.lengthOf(2);
                        done();
                    })
                })
            } catch (error) {
                done(error);
            }
        });
        it('Test 9: Ubacivanje redovnog zauzeca koje vec postoji: ', function (done) {
            let zauzece = {
                datum: '05.03.2019',
                pocetak: '09:00',
                kraj: '10:00',
                naziv: '1-11',
                predavac: 'Drugi Neko',
                periodicnost: true
            }
            try {
                request({
                    url: 'http://localhost:8080/rezervacije.html',
                    method: "POST",
                    headers: {
                        "content-type": "application/json",
                    },
                    body: zauzece,
                    json: true
                }, function (error, response, body) {
                    request('http://localhost:8080/zauzeca', function (error, response, body) {
                        let zauzeca = JSON.parse(body);
                        expect(zauzeca.periodicna).to.have.lengthOf(2);
                        done();
                    })
                })
            } catch (error) {
                done(error);
            }
        });
        it('Test 10: Ubacivanje vanrednog zauzeca koje vec postoji(druga osoba): ', function (done) {
            let zauzece = {
                datum: '12.12.2019',
                pocetak: '17:00',
                kraj: '19:00',
                naziv: '1-15',
                predavac: 'Drugi Neko',
                periodicnost: false
            }
            try {
                request({
                    url: 'http://localhost:8080/rezervacije.html',
                    method: "POST",
                    headers: {
                        "content-type": "application/json",
                    },
                    body: zauzece,
                    json: true
                }, function (error, response, body) {
                    request('http://localhost:8080/zauzeca', function (error, response, body) {
                        let zauzeca = JSON.parse(body);
                        expect(zauzeca.vanredna).to.have.lengthOf(3);
                        done();
                    })
                })
            } catch (error) {
                done(error);
            }
        });
    });
});
