const db = require('./baza.js')
function initialize() {
    db.sequelize.sync({ force: true }).then(function () {
        inicializacija().then(function () {
            process.exit();
        });
    });    
}

//Inicijalizacija podataka
function inicializacija() {
    //Lista promise-a
    let osobljeListaPromisea = [];
    let saleListaPromisea = [];
    let terminiListaPromisea = [];
    let rezervacijaListaPromisea = [];
    return new Promise(function (resolve, reject) {
        //Osoblje
        osobljeListaPromisea.push(db.osoblje.create({ id: 1, ime: 'Neko', prezime: 'Nekic', uloga: 'profesor' }));
        osobljeListaPromisea.push(db.osoblje.create({ id: 2, ime: 'Drugi', prezime: 'Neko', uloga: 'asistent' }));
        osobljeListaPromisea.push(db.osoblje.create({ id: 3, ime: 'Test', prezime: 'Test', uloga: 'asistent' }));
        Promise.all(osobljeListaPromisea).then(function (osoblje) {
            let profesor = osoblje.filter(function (a) { return a.id == '1' })[0];
            let asistent = osoblje.filter(function (a) { return a.id == '2' })[0];
            let asistent2 = osoblje.filter(function (a) { return a.id == '3' })[0];
            //Sale
            saleListaPromisea.push(db.sala.create({ id: 1, naziv: '1-11' }).then(
                function (sala) {
                    sala.setOsoba(profesor);
                    return new Promise(function (resolve, reject) { resolve(sala); });
                }
            ));
            saleListaPromisea.push(db.sala.create({ id: 2, naziv: '1-15' }).then(
                function (sala) {
                    sala.setOsoba(asistent);
                    return new Promise(function (resolve, reject) { resolve(sala); });
                }
            ));
            Promise.all(saleListaPromisea).then(function (sale) {
                let sala = sale.filter(function (s) { return s.id == '1' })[0];
                //Termini - termin za 2020 godinu je postavljen na 2019 jer je kalendar napravljen za 2019 godinu
                terminiListaPromisea.push(db.termin.create({ id: 1, redovni: false, datum: '01.01.2019', pocetak: '12:00', kraj: '13:00'}));
                terminiListaPromisea.push(db.termin.create({ id: 2, redovni: true, dan: 0, semestar: 'zimski', pocetak: '13:00', kraj: '14:00'}));
                Promise.all(terminiListaPromisea).then(function (termini) {
                    let vanredni = termini.filter(function (t) { return t.id == '1'; })[0];
                    let redovni = termini.filter(function (t) { return t.id == '2'; })[0];
                    //Rezervacije
                    rezervacijaListaPromisea.push(db.rezervacija.create({id: 1}).then(function (rezervacija) {
                        rezervacija.setVrijeme(vanredni);
                        rezervacija.setGdje(sala);
                        rezervacija.setKo(profesor);
                        return new Promise(function (resolve, reject) { resolve(rezervacija); });
                    }));
                    rezervacijaListaPromisea.push(db.rezervacija.create({id: 2}).then(function (rezervacija) {
                        rezervacija.setVrijeme(redovni);
                        rezervacija.setGdje(sala);
                        rezervacija.setKo(asistent2);
                        return new Promise(function (resolve, reject) { resolve(rezervacija); });
                    }));
                });
            });
        })
    });
}


module.exports.initialize = initialize;